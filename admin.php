<?php 
    include('functions/functions.php');
    $id = $_GET['id'];
?>
<!doctype html>
<html>
    <head>
        <meta char="UTF=8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Address Book</title>
        <link rel="stylesheet" type="text/css" href="css/main.css" >
        
    </head>
    <body>
        <section class="background"></section>
        <header>
            Address Book
        </header>
        <nav><ul class="navBar" id="nav"></ul></nav>
        <section class="container">
            <div class="centerBox">
               <div class="centerText"> This page is under construction <br> <a href="index.php">click here to return to the home page</a></div>
            </div>
        </section>
        <script src="js/scripts.js"></script>
    </body>
</html> 