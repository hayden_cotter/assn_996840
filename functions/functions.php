<?php 

    include_once('xyz.php');

    function connection() {
        
        $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
        
        if ($conn->connect_errno > 0) {
            die('Unable to connect to database ['.$conn->connect_errno.']');
        }
        
        return $conn;
    }
    
    //Get Name is side column
    function get_side_column() {
        
        $db = connection();
        $sql = "SELECT ID, FNAME, LNAME FROM tbl_contacts ORDER BY ID";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "fname" => $row['FNAME'],
                "lname" => $row['LNAME']
            );
        }
        
        $json = json_encode(array($arr));
        
        $result->free();
        $db->close();
        
        return $json;
    }
    
    function set_side_column($data) {
        
        $array = json_decode($data, True);
        
        $output = "";
        
        if (count($array[0]) > 0 ) {
            for ($i = 0; $i < count($array[0]); $i++) {
                $output .= "<li><a href='index.php?id=".$array[0][$i]['id']."'>".$array[0][$i]['fname']." ".$array[0][$i]['lname']."</a></li>";
            }
            return $output;
        }
        else {
            $output .= "<li>No Entries</li>";
            return $output;
        }
    }
    
    //Get Name is center area
    function get_data($id) {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_contacts WHERE ID = ".$id;
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "fname" => $row['FNAME'],
                "lname" => $row['LNAME'],
                "address" => $row['ADDRESS'],
                "phone" => $row['PHONE'],
                "email" => $row['EMAIL']
            );
        }
        
        $json = json_encode(array($arr));
        
        $result->free();
        $db->close();
        
        return $json;
    }
    
    function showData($data, $page) {
        
        $array = json_decode($data, True);

        $output = "";
        
        if (count($array[0]) > 0 ) {
            for ($i = 0; $i < count($array[0]); $i++) {

                if ($page == "index") {
                    //String for HTML table code
                    $output .= "<tr><td>".$array[0][$i]['fname']."</td><td>".$array[0][$i]['lname']."</td><td>".$array[0][$i]['address']."</td><td>".$array[0][$i]['phone']."</td><td>".$array[0][$i]['email']."</td></tr>";
                }
                
                if ($page == "admin") {
                    //String for HTML table code
                    $output .= "<tr><td>".$array[0][$i]['fname']."</td><td>".$array[0][$i]['lname']."</td><td>".$showDob."</td><td class='age'>".$dateDiff."</td><td><a href=\"edit.php?id=".$array[0][$i]['id']."\">Edit</a></td><td><a href=\"functions/process3.php?id=".$array[0][$i]['id']."\">Delete</a></td></tr>";    
                }
                
                   
            }
            
            return $output;
        }
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

?>