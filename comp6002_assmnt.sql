-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 16, 2016 at 10:06 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `comp6002_assmnt`
--
CREATE DATABASE IF NOT EXISTS `comp6002_assmnt` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `comp6002_assmnt`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contacts`
--

DROP TABLE IF EXISTS `tbl_contacts`;
CREATE TABLE `tbl_contacts` (
  `ID` int(11) NOT NULL,
  `FNAME` varchar(255) NOT NULL,
  `LNAME` varchar(255) NOT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `PHONE` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contacts`
--

INSERT INTO `tbl_contacts` (`ID`, `FNAME`, `LNAME`, `ADDRESS`, `PHONE`, `EMAIL`) VALUES
(1, 'Francesca', 'Hogan', '6508 Neque. Avenue', '(811) 138-2310', 'In.ornare.sagittis@nonummyutmolestie.com'),
(2, 'Calvin', 'House', '2909 Mauris Rd.', '(838) 149-7052', 'scelerisque@ultricesDuis.com'),
(3, 'Guy', 'Mccarthy', 'Ap #526-8661 Nullam Road', '(874) 411-4045', 'vestibulum.neque.sed@metusVivamuseuismod.edu'),
(4, 'Bryar', 'Reilly', '939-1159 Vestibulum Rd.', '(983) 105-7532', 'turpis@semmagnanec.co.uk'),
(5, 'Deborah', 'Skinner', 'Ap #674-3367 Diam. Street', '(819) 476-5425', 'nec@Donec.com'),
(6, 'Genevieve', 'Sargent', 'P.O. Box 762, 3266 Ac Rd.', '(666) 822-1626', 'libero.est@diamDuismi.co.uk'),
(7, 'Dexter', 'Sexton', '776-2205 Consectetuer Road', '(687) 113-9535', 'lobortis.mauris@molestiesodalesMauris.org'),
(8, 'Linda', 'Dyer', '8754 Mus. Road', '(175) 917-3491', 'Quisque.ac.libero@facilisis.ca'),
(9, 'Vielka', 'Mullen', 'P.O. Box 392, 8432 Proin Av.', '(199) 342-8920', 'metus@sedpede.net'),
(10, 'Clarke', 'Hill', '1224 Aliquam Av.', '(736) 571-9227', 'ultrices.iaculis@aliquetmagnaa.com'),
(11, 'Evelyn', 'Bell', 'P.O. Box 253, 158 Sit Avenue', '(619) 775-1664', 'Vestibulum.ut@Proin.org'),
(12, 'Adara', 'Shaffer', 'P.O. Box 452, 3728 Euismod Rd.', '(782) 446-0709', 'tempor.erat@sagittis.com'),
(13, 'Winifred', 'Snider', '806-1215 Ac Rd.', '(348) 312-0157', 'dictum@accumsansed.edu'),
(14, 'Vielka', 'Perry', '6079 Enim. Av.', '(922) 328-9438', 'vestibulum@volutpatornarefacilisis.com'),
(15, 'Xerxes', 'Munoz', '684-5614 Aliquam Rd.', '(952) 899-2620', 'mollis.dui@ultricesDuis.com'),
(16, 'Mannix', 'Goodwin', 'Ap #217-2850 Nisi St.', '(478) 749-2210', 'gravida.Aliquam.tincidunt@ullamcorper.ca'),
(17, 'Keelie', 'Atkins', '317-9874 Mauris Ave', '(993) 698-6680', 'feugiat.Lorem@ultricesa.com'),
(18, 'Lars', 'Nash', 'Ap #368-7241 Lorem Rd.', '(834) 380-0453', 'nascetur.ridiculus.mus@quispedeSuspendisse.ca'),
(19, 'Imelda', 'Burt', 'P.O. Box 718, 1954 Ac St.', '(165) 392-4549', 'Fusce.diam.nunc@sitametmetus.com'),
(20, 'Brennan', 'Valenzuela', '2144 Montes, Rd.', '(264) 308-6927', 'malesuada@nibhAliquamornare.net'),
(21, 'Lael', 'Woodard', '8153 Tempus Rd.', '(437) 121-6002', 'nec.luctus.felis@pharetraQuisqueac.org'),
(22, 'Moana', 'Gill', '720-5214 Phasellus Road', '(250) 807-1210', 'non.dapibus.rutrum@et.net'),
(23, 'Yuri', 'Fitzgerald', 'P.O. Box 355, 9773 In Street', '(307) 323-7113', 'Maecenas@fermentumrisusat.com'),
(24, 'Judah', 'Nash', 'Ap #326-1718 Et Rd.', '(799) 613-2404', 'sit.amet.consectetuer@semperrutrum.co.uk'),
(25, 'Amena', 'Maynard', 'Ap #113-9689 Nibh Street', '(733) 284-5726', 'sem.Pellentesque@tempor.edu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_contacts`
--
ALTER TABLE `tbl_contacts`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_contacts`
--
ALTER TABLE `tbl_contacts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
