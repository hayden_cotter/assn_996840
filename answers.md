**11  
In PHP we split up the get and set functions to ensure that values are not being changed when we do not want them to be.  
  
**12  
With the display:flex property we are given the functionality to choose the order the flex objects appear.  
  
**13  
The line:  
document.getElementById("nav").innerHTML = showNav();  
Is used to set the inner HTML (content between the brackets) of the tags (an open and a closing) with the id of 'nav'.  
The content it is setting it to is the result of the 'showNav' function.  
  
**14  
The data in xyz.php is in a serperate file for these 2 reasons:  
1: Security, xyz.php includes the username and password to connect to our database, having it seperate allows it to be secured better.  
2: Repeatablity, the contents of xyz.php are used any time we wish to connect to our database, this could occur on multiple pages on our website, having a seperate file allows us to just include the file instead of retyping the contents.  